#!/usr/bin/env python
# coding: utf-8

# # HW1
# ## Özgür Özer 090160717
# ### Newton-Raphson Method

from sympy import *

x = symbols("x")


def newton(f, y, tol = 1e-3):
    """ Newton-Raphson method is a mathematical method to spot a root of a function near a point using mean value theorem.
        Thus, it inherits the drawbacks of mean value theorem. As a result, choose the y value within the interval y has the same
        differentiation sign as that of the root be found near.

        f: expression whose roots to be found
        y: point near the root to search the root around
        tol: tolerance of the calculation maximum error tolerable
    """
    while abs(f.evalf(subs={x: y})) > tol:
        # here sympy functionality is used only once to elude hard coded differentiation, so differentiation is the only symbolic part. The rest is numeric.
        y = y - f.evalf(subs={x: y}) / diff(f,x).evalf(subs={x: y})
    return y

# Define your function with x and give it to the newton function
df = x**2 - 4*x - 7
print(newton(df, 5))
print(newton(df, 5, 1e-6))
print(newton(df, 5, 1e-12))
print(newton(df,-5)) # read the docstring
print(newton.__doc__)

